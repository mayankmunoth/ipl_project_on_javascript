const csv =require("csvtojson");
const matchesFilePath = "CSV_Data/matches.csv";
const deliveryFilePath="CSV_Data/deliveries.csv";

function main(){
    csv()
     .fromFile(matchesFilePath)
     .then(matches => {
        csv()
        .fromFile(deliveryFilePath)
        .then(deliveries => {
            findMatchesPlayedPerYear(matches);
            findMatchesWonByEveryTeam(matches);
            findExtraRunsIn2016byEachTeam(matches,deliveries);
            findTopeconomyBowlerOf2015(matches,deliveries);
            findMatchesPlayedEveryVenue(matches);

        });
      });
    }
    main();

    function findMatchesPlayedPerYear(matches) {
      const matchesPlayed = {};

      for (let match of matches) {
        const season = match.season;
        if (matchesPlayed[season]) {
          matchesPlayed[season] += 1;
        } else {
          matchesPlayed[season] = 1;
        }
      }
       console.log("Total Matches Played Every Season")
       console.log(matchesPlayed);  
    }


    function findMatchesWonByEveryTeam(matches) {
      const matchesWon = {};
      
      for (let match of matches) {
        const matchesWin = match.winner;
        if (matchesWon[matchesWin]) {
          matchesWon[matchesWin] += 1;
        } else {
          matchesWon[matchesWin] = 1;
        }
      }
       console.log("Total Matches won every team ")
       console.log(matchesWon);  
    }

    function findExtraRunsIn2016byEachTeam(matches, deliveries) {
    let extraRun={};
    for(match of matches){
        for(delivery of deliveries){
            if(match.id==delivery.match_id && match.season=='2016'){
                if(extraRun[delivery.bowling_team]){
                  extraRun[delivery.bowling_team]+=parseInt(delivery.extra_runs);
                }
                else{
                  extraRun[delivery.bowling_team]=parseInt(delivery.extra_runs);
                }
            }
        }
    }
    console.log("Extra run conduct every team in 2016")
    console.log(extraRun);
}


function findTopeconomyBowlerOf2015(matches,deliveries) {
        var overs={};
        var totalRuns={};
        
        for(let match of matches){
         for(let delivey of deliveries)  {
            if(match.id===delivey.match_id && match.season=='2015'){
                if(totalRuns[delivey.bowler]){
                    totalRuns[delivey.bowler]+=parseInt(delivey.total_runs);
                    if(delivey.ball=='1'){
                        overs[delivey.bowler]+=1;
                    }
                }
                else{
                    totalRuns[delivey.bowler]=parseInt(delivey.total_runs);
                    overs[delivey.bowler]=1;
                }
             }
          }
       } 
        var economyBowlers={};
        for(let i in totalRuns) {
          economyBowlers[i]=parseFloat((totalRuns[i]/overs[i]).toFixed(2));    
        }

        let sortEconomyBowlers=Object.values(economyBowlers);
        sortEconomyBowlers.sort(function(a,b){ return a-b });

        var topEconomyBowlers={};
        for(let i=0;i<5;i++){
            for(let j in economyBowlers){
                if(economyBowlers[j]==sortEconomyBowlers[i]){
                    topEconomyBowlers[j]=sortEconomyBowlers[i];
                }
            }
        }
        console.log("Top Economy bowler in year of 2015");
        console.log(topEconomyBowlers);
    }

    function findMatchesPlayedEveryVenue(matches) {
      const matchPlayed = {};
      for (let match of matches) {
        const venue = match.venue;
        if (matchPlayed[venue]) {
          matchPlayed[venue] += 1;
        } else {
          matchPlayed[venue] = 1;
        }
      }
      console.log("Matches Played in every venue");
       console.log(matchPlayed);
    }